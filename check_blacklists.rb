base_blacklists = [
  "https://adaway.org/hosts.txt",                  
  "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext",                                 
  "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts",
  "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews/hosts",                                          
  "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/gambling/hosts",                                          
  "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/porn/hosts",                                              
  "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/social/hosts",                                            
  "https://badmojr.gitlab.io/1hosts/mini/domains.wildcards",
  "https://badmojr.gitlab.io/1hosts/Lite/domains.wildcards",
  "https://badmojr.gitlab.io/1hosts/Pro/domains.wildcards",
  "https://badmojr.gitlab.io/1hosts/Xtra/domains.wildcards",
  "https://dbl.oisd.nl/basic/",
  "https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Hosts/GoodbyeAds.txt",
  "https://raw.githubusercontent.com/Spam404/lists/master/main-blacklist.txt",
  "https://urlhaus.abuse.ch/downloads/hostfile/",
  "https://someonewhocares.org/hosts/hosts",
  "https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt",
  "https://www.github.developerdan.com/hosts/lists/ads-and-tracking-extended.txt"
]

require "net/http"
require "uri"
require "json"
require "thread"
require_relative "parser"

def get_blacklists api
  list_list = Net::HTTP.get URI(api)
  data = JSON.load(list_list)
  
  blacklists = []
  data.each { |list|
    blacklists << list["primaryViewUrl"]
  }
  
  return blacklists
end

puts "Collecting blacklists..."
blacklists = get_blacklists "https://filterlists.com/api/directory/lists"

blacklists.push *base_blacklists

yggdrasil_domains = get_whitelist

$founds = []
$founds_mutex = Mutex.new

def check_list name, list, domains, progress
  found = false
  domains.each { |domain|
    if list.include? domain
      found = true
      text = "-----> Found yggdrasil peer #{domain} in blacklist #{name}"
      puts text
      $founds_mutex.synchronize {
        $founds << text
      }
    end
  }
  
  if ! found
    puts "Found no yggdrasil peer in blacklist (#{progress}) #{name}"
  end
end

threads_n = 50
threads = []


blacklists.each_slice(blacklists.length / threads_n).each_with_index { |subblacklists, th_n|
threads << Thread.new(th_n) { |th_n|
  subblacklists.each_with_index { |blacklist, index|
    begin
        list = Net::HTTP.get URI(blacklist)
        check_list blacklist, list, yggdrasil_domains, "TH#{th_n + 1} - #{index + 1} of #{subblacklists.length} blacklists"
    rescue
        puts "Failed to check blacklist: #{blacklist}"
    end
  }
}
}

threads.each { |th|
    th.join
}

puts "Founds:"
puts $founds.join("\n")

puts "Complete!"


