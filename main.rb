
$whitelist_prefix = "!"

require_relative "parser"

dns_whitelist = get_whitelist

puts "Writing whitelist..."

dns_whitelist.sort!

whitelist_file = File.new "dns_whitelist.txt", "w"
dns_whitelist.each { |domain|
  whitelist_file.puts "#{$whitelist_prefix}#{domain}"
}
whitelist_file.close

puts "Complete!"
