def yggdrasil_get_peers row_data
  res = row_data.scan(/\s+\* `(?:tcp|tls):\/\/(.*?)\:\d{1,5}(?:\?key=.{64})?(?:\&sni=[\w\d\.]+?)?`/)
  if res.empty?
    return res
  end

  # remove inner array
  res.map! { |peer| peer[0] }

  return res
end

def get_whitelist

  yggdrasil_peers = []

  blacklisted_files = ["i2p.md", "tor.md", "lokinet.md"]
  Dir["./yggdrasil-peers/*/*.md"].each { |file|
    basename = File.basename file
    if blacklisted_files.include? basename
      # skip entry
      next
    end
    row_data = File.read file
    peers = yggdrasil_get_peers row_data

    puts "Found #{peers.length} peers in peer list #{basename}"

    yggdrasil_peers.push *peers
  }

  # remove duplicates
  yggdrasil_peers.uniq!
  puts "=> Found #{yggdrasil_peers.length} yggdrasil peers"

  dns_whitelist = []
  yggdrasil_peers.each { |peer|
    if peer =~ /^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/
      # ipv4
    elsif peer =~ /(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))/
      # ipv6
    else
      # domainname
      dns_whitelist << peer
    end
  }

  puts "=> Found #{dns_whitelist.length} dns names"

  return dns_whitelist
end