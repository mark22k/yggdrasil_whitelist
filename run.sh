echo "Syncing yggdrasil peers..."
git clone https://github.com/yggdrasil-network/public-peers.git yggdrasil-peers
cd yggdrasil-peers
git pull
cd ..


echo "Creating whitelist..."
ruby main.rb