# Yggdrasil whitelist creator

When using DNS blacklisting and Yggdrasil, it may happen that Yggdrasil peers are on these blacklists. Often DNS blacklist tools like AdGuard offer the possibility to create a whitelist. This repo contains a small script that fetches all DNS names from the public-peers repo and writes them to a file.

## Dependencies

- Ruby
- SH

## Usage

In the file main.rb you can define a prefix in line two, which precedes each domain entry. For example, the prefix `"!"` is necessary for the app personalDNSfilter. With AdGuard you can set this to `""`:
```
$whitelist_prefix = "!"
```

After that you can run `sh run.sh`. The script automatically loads the public-peers repo and then runs main.rb which generates a file `dns_whitelist.txt` which contains the entries.
